/*Distributed Intelligence Agent/Register
 * By Sebastian Zaba
 * Winter CSC 435
 * Register Server keeps track of available hosts to send agents to
 * Register will wait until there are more then 2 agents and then send them
 * all of to a gameServer.
 * Or there are two types of Agents ones with status 1 "employed" or status 0 "scouts"
 * employed bees have a favorite server and will advocate to come play with them at that server
 * scouts do not have a favorite server and are asking for a favorite. Agents become employed
 * if they have won at the last server. If more then 4 scouts only or 4<employed agents 
 * are queued the server sends them of to the first registered server and move them to the back of the line 
 * 
 * 
 * 
 * 
 */

import java.io.*;
import java.net.*;
import java.util.*;
import com.thoughtworks.xstream.XStream;


class HTMLFront extends Thread {
	Socket sock; // create a socket

	HTMLFront(Socket s) {
		sock = s;
	}
	
	public void run() {
		
		try {
		
			PrintStream outAS = new PrintStream(sock.getOutputStream()); //print stream to browser
			BufferedReader in = new BufferedReader(new InputStreamReader(sock.getInputStream())); //read from browser
			String registerRequest = in.readLine();				//Strings send from browser										
							
	
			 if(registerRequest !=null && registerRequest.contains("check")){
				 Register.masterList.checkServers();
				 outAS.println("Available Servers: " + Register.masterList.keys);
			 }
			 
			 else if(registerRequest !=null && registerRequest.contains("agents")){
			//	outAS.println("Hello");
				 
				 StringBuilder HTMLList = new StringBuilder(); 
				 HTMLList.append("<HTML>");
				 for (Agent a: agentTracker.agentsHTML){
				 HTMLList.append(a.getAgentName() + " has been: " + a.getGameHistory().toString()+"<p>");
				 }
				 HTMLList.append("</HTML>");
		//		 System.out.println(HTMLList.toString());
				 sendHTML(HTMLList.toString(), outAS);
			 }	 
		
			else{
				outAS.println("Invalid Request :(");
			}
			 
			 sock.close(); //let server know we are done
		} catch (IOException e) {
			System.out.print(" Error code line 20!" + e);
		}
		
	}

	 static void sendHTML(String html, PrintStream out) {
			
			out.println("HTTP/1.1 200 OK");
			out.println("Content-Length: " + html.length());
			out.println("Content-Type:" + mimer(html));
			out.println("");		
			out.println(html); 
		}
	
	private static String mimer(String message) {
				
		String content;
		{
			
			// logic taken from 150 line server
			// http://cs.au.dk/~amoeller/WWW/javaweb/server.html and modified to
			// fit needs
			if (message == null)
				return "null was requested";
			
			if (message.endsWith(".html") || message.endsWith(".htm"))
				content = "text/html";
			else if (message.endsWith(".txt") || message.endsWith(".java"))
				content = "text/plain";
			else if (message.endsWith(".gif"))
				content = "image/gif";
			else if (message.endsWith(".class"))
				content = "application/octet-stream";
			else if (message.endsWith(".jpg") || message.endsWith(".jpeg"))
				content = "image/jpeg";
			else if (message.endsWith(".zab")) //added to return custom mime type
				content = "application/zab";
			else if (message.endsWith(".xyz")) //added to return custom mime type
				content = "application/xyz";
			else
				content = "text/html";
		}

		return content;
	}

	/*
	 * Used to update and save the ServerList in case the server goes down
	 * 
	 */
	public static void save(Object toBeSaved) throws IOException{
		
		FileOutputStream fis =  new FileOutputStream("hostTracker.obj"); //save the server list
		
		ObjectOutputStream objectSave = new ObjectOutputStream(fis);
		
		objectSave.writeObject(toBeSaved);
		objectSave.close();
		
	}
	
}
	
public class Register {
	//Start server and spawn threads to deal with browser requests
	public static boolean controlSwitch = true;
	public static int waitCounter;	
	//static hostTracker masterList = (hostTracker) open("hostTracker.obj"); //later to save in case it goes down
	static hostTracker masterList = new hostTracker();
	static agentTracker agentList = new agentTracker();
	public static void main(String a[]) throws IOException {
	//	hostTracker masterList = new hostTracker();
	//	Spawn.save(masterList);
		//start of Listener for requests.
		Listener  t = new Listener();
		t.start();
		
		
		
		ApplicationIncoming AI = new ApplicationIncoming();
		Thread t2 = new Thread(AI);
		t2.start();
				
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Sebastian Zaba's Master DIA");
		
		if(in.readLine().contains("quit"));
		controlSwitch = false;

	}
	
	public static Object open(String fileLocation){
		
		FileInputStream fis = null;
		ObjectInputStream in = null;
		Object restored = null;
		
		try {
			fis = new FileInputStream(fileLocation);
			in = new ObjectInputStream(fis);
			restored = in.readObject();
			in.close();
			return restored;
		} catch (IOException | ClassNotFoundException e) {e.printStackTrace();}
				
		return restored;
	
}
}

//keeps track of servers and is being saved after updates
class hostTracker implements Serializable{
	
	/**
	 * sending ID
	 */
		
	private static final long serialVersionUID = 4197098629056270891L;
	
	int totalServers =0; //keep track registered servers
	    //address of remote servers ip + port
	Queue<String> keys = new LinkedList<String>();
	private HashMap<String,ServerDetail> locName = new HashMap<>();
	
	//to register server we need a servername (unchecked for duplicates!!), your digits for access with port
	//and an output stream to send the sender a nice postcard as ack
	public void registerServer(ServerDetail obj, PrintStream outHT){
		
		totalServers++;
		keys.add(obj.getName());
		locName.put(obj.getName(), obj);
		outHT.println(obj.getName() + " registered at Address: " + obj.getIP());
		outHT.println(keys);
	}
	
	public void RemoveServer(String access){
		keys.remove(access);
		locName.remove(access);
	}
	public void RemoveAll(){
		keys.removeAll(keys);
		
	}
	
	public HashMap<String, ServerDetail> AvailableServers(){
		//send
		
		return locName;
	}

	public void checkServers() throws UnknownHostException {
		PrintStream outCS;
		ArrayList<String> dead = new ArrayList<String>();
		for (String key : keys){
			ServerDetail testingServer = locName.get(key);
			System.out.println(testingServer);
			
			
			Socket sock;
			try {
				sock = new Socket(testingServer.getIP(), testingServer.getPort()); 					
			//	BufferedReader in =  new BufferedReader(new InputStreamReader(sock.getInputStream()));
			outCS = new PrintStream(sock.getOutputStream());
			outCS.println("Hello, you there");		
			
			} catch (IOException e) {System.out.print(e + " removing Server, no response "); 
			dead.add(key);}
			
		}
		System.out.println(dead.toString());
		for (String noResponse : dead){
			keys.remove(noResponse);
		}
			System.out.println(keys);	
	}
	//search for requested Server
	public ServerDetail findServer(String serverName){
	
		if(locName.containsKey(serverName)){
	    ServerDetail foodSource = locName.get(serverName);//get requested server		
	    return foodSource;
		}
		
		else{
		System.out.println("Server not found!");
		return null;
		}
	}
	//provide a Random Server
	public ServerDetail provideAvailableGameServer(){
		
		
		try{
		
		String get = keys.poll();
		keys.add(get);
		return locName.get(get);
		
		}catch(NullPointerException | IllegalArgumentException e ){System.out.println("No Servers Registered!");}
			
		return null;
		
	}

		//send agent to requested server
	public void sendAgent(Agent object, String serverName){
		
		XStream xstream = new XStream();		
		Socket sock;
		//find server and return the object
		System.out.println(serverName);
		ServerDetail sendHere = findServer(serverName);
			
		try {
				
		sock = new Socket(sendHere.getIP(), sendHere.getPort());
			
		xstream.alias("agentDetail" , Agent.class);
		String xml = xstream.toXML(object); 
				    
		if(!xml.isEmpty()){
	    PrintStream out = new PrintStream (sock.getOutputStream());
	    out.println(xml);
	    out.println("end_of_xml");
	    System.out.println("SEND! " + sendHere.getIP());
		}
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}
}

//XMLAcceptor threads which handles xml data send via file
	class XMLAcceptor extends Thread {
	    
		private Socket sock;
	    
	    private int i;
	    //accept agents at 2555
	    XMLAcceptor (Socket s){sock = s;}
	    PrintStream out = null; BufferedReader in = null;

	    String xml; //String which will hold the send xml file
	    String temp; //hold the temporary String while building xml
	    XStream xstream = new XStream(); //the xml "marshall" 
	    final String newLine = System.getProperty("line.separator");
	    
	    public void run(){
	      System.out.println("Ready to accept XML");
	      try{
		in =  new BufferedReader(new InputStreamReader(sock.getInputStream()));
		out = new PrintStream(sock.getOutputStream()); // to send ack back to client
		i = 0; xml = "";
		while(true){
		  temp = in.readLine();
		  if (temp.indexOf("end_of_xml") > -1) break;
		  else xml = xml + temp + newLine; // Should use StringBuilder in 1.5 
		}
		System.out.println(xml);
		//check if xml is agent
		if(xml.contains("agentDetail")){
		xstream.alias("agentDetail", Agent.class);
	    Agent incoming = (Agent) xstream.fromXML(xml); //an agent object is born
		out.println("Hello " + incoming.getAgentName());
	    out.println("ACK"); //ack
	    //send to first registered server for testing
	    incoming.addGameHistory("Sebastian's Hive");
	    System.out.println(incoming.getAgentName() + " fused from XML to Agent");
	    Hive.enterHive(incoming);
	    	    
		}
		
		//check if server to be registered
		else if(xml.contains("serverDetail")){
			
			xstream.alias("serverDetail", ServerDetail.class);
			ServerDetail reg = (ServerDetail) xstream.fromXML(xml);
			out.print(reg.getName() + " is now official! ");
			Register.masterList.registerServer(reg, out);
			System.out.println(reg.getName() + " is online! ");
		}
		out.flush(); 
		sock.close();
		
	      }catch (IOException | InterruptedException e){
	      } // end run
	    }
	}
	
	//send to XMLAgentAcceptor to unmarshal XML Agents
	class ApplicationIncoming extends Thread {
		
		public void run(){
		
		int port = 2555;
		Socket sock;
		try{	
			
			 ServerSocket servsock = new ServerSocket(port);
		      while (Register.controlSwitch) {
		    	
			// wait for the next ADMIN client connection:
		    	sock = servsock.accept(); // servsock stays open waiting for incoming request
				XMLAcceptor xa = new XMLAcceptor(sock);
		    	
		    	xa.start();
		    	xa.join(); //join used to avoid concurrency issues of threads families it waits
		    			   //until previous thread dies
		      }
		      servsock.close();
		    }catch (IOException | InterruptedException ioe) {System.out.println(ioe);}	
		}
	}
	
	//listener is for HTML interface currently /agents actually being used
	class Listener extends Thread {
				 		
	public void run(){	
		int q_len = 6;
		int port = 2540;
		Socket sock; 
		
		try{
				
		
		      ServerSocket servsock = new ServerSocket(port, q_len);
		      while (Register.controlSwitch) {
		    	sock = servsock.accept(); // servsock stays open waiting for incoming request
				new HTMLFront(sock).start();; 
		      }
		      servsock.close();
		    }catch (IOException ioe) {System.out.println(ioe);}
 
      }
	}
	
	//all the logic happens here
	class Hive extends Thread{
			
			
		public void run() {
			
			System.out.println("Hive ready for colaboration!");
			//increment wait counter
			Register.waitCounter++;
			
			//to shorten bounces between servers
			if(!agentTracker.getEmployed().isEmpty() && agentTracker.peekEmployed().getGameHistory().size()>5){			
			agentTracker.pollEmployed();
			}
			else if(!agentTracker.getScout().isEmpty() && agentTracker.peekScout().getGameHistory().size()>5){
			agentTracker.pollScout();
			}
			
			// if there are scouts looking for foodsources and employed bees to share information then
			while(agentTracker.getEmployed().size() >= 1 && agentTracker.getScout().size()>=1){
				
					
					
				//the employed bee/player joins the scout and goes for a game
				String fav = agentTracker.peekEmployed().getGameHistory().get(0); //get favorite server at position 0
				Register.masterList.sendAgent(agentTracker.peekScout(),fav );
				agentTracker.pollScout();
				Register.masterList.sendAgent(agentTracker.peekEmployed(), fav);
				agentTracker.pollEmployed();
				}
				//if more than 2 players are waiting send them out to available servers	
				//speed up demo
			if (Register.waitCounter > 2){
									
					while(!agentTracker.getScout().isEmpty()){	
						//to shorten the demo the agent will do 5 iterations and stop
						
					Register.masterList.sendAgent(agentTracker.peekScout(), Register.masterList.provideAvailableGameServer().getName());				
					agentTracker.pollScout();
					}
					while(!agentTracker.getEmployed().isEmpty()){
						
	
						Register.masterList.sendAgent(agentTracker.peekEmployed(), Register.masterList.provideAvailableGameServer().getName());
						agentTracker.pollEmployed();
					}
					//reset waitCounter
					Register.waitCounter = 0;
				}								
			}
			
		public static void enterHive(Agent bee) throws InterruptedException{			
		//if agent has no favorite server add to scout
			if(bee.getStatus() ==0){
			agentTracker.addScout(bee);	
			agentTracker.agentsHTML.add(bee);//add to arrayLog for display
		}
			//if agent has favorite server add to employed
		else if(bee.getStatus() == 1){
			agentTracker.addEmployed(bee);
			agentTracker.agentsHTML.add(bee); //add for html display
		}
		
//		try {
		Hive h = new Hive();
		
		h.start();
		h.join(); //again wait until previous thread dies keeps a nice order	
		System.out.println("Bee has entered!"); //let Register now we have new bee
		}
		
		
	}
	//since this is a shared object its synchronized to avoid concurrency conflicts
	class agentTracker{
		
		private static Queue<Agent> scout = new LinkedList<>(); //agents with status 0 looking for foodsource or lost on server														//depleted food source
		private static Queue<Agent> employed = new LinkedList<>(); //agents with favorite foodsource status 1
		static ArrayList<Agent> agentsHTML = new ArrayList<>();
			
		public static synchronized Agent peekScout(){
			return scout.peek();
		}
		public static synchronized void pollScout(){
			scout.poll();
		}
		public static synchronized Agent peekEmployed(){
			return employed.peek();
		}		
		public static synchronized void pollEmployed(){
			employed.poll();
		}
		public static synchronized Queue<Agent> getScout(){
			
			return scout;
		}
		public static synchronized Queue<Agent> getEmployed(){			
			return employed;
		}
		public static synchronized void addScout(Agent a){
			scout.add(a);
		}
		public static synchronized void addEmployed(Agent a){
			employed.add(a);
		}
	}

	