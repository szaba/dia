
/* Sebastian Zaba
 * Used to DIA Project CSC 435 Winter 2013
 * Used to demonstrate Intelligent Agents that migrate using the ABC Algorithm
 * Agents come in an a preset port that is registered with the Register/Name Server
 * Agents also come in as XML and are being parsed into objects and back into XML when 
 * send back.
 * Agents play their set amount of Games.
 * The set table size waits for enough players and then randomly assigns winners per game
 * and increases or decreases the stack.(Simple Logic non trivial to concept)
 * The winners will set this GameServer as favorite and loosers will demote this server
 * by setting their favorite to 0. They both then get shipped back to the Register Server
 * 
 * For ease after hitting enter this GameServer can send Agents to the NameServer
 * advocating for this Server by having it preset to this gameserver and status 1
 * making them employed.
 * 
 * XML parsing is done thanks to the codehaus XStream library
 */
import java.io.*;
import java.net.*;
import java.util.*;

import com.thoughtworks.xstream.XStream;

public class GameServer {
	
	static Boolean controlSwitch = true;	
	static private ArrayList<Agent> table = new ArrayList<Agent>();
	static Queue<Agent> gameList = new LinkedList<>();
	static int namePort;
	static String nameServer;
	static String hallName;
	static int gamePort;
	static String gameIP;
	
	//synchronized methods for concurrency conflicts during high traffic
	public static synchronized void addTable(Agent a){
		table.add(a);		
	}
	public static synchronized Agent getTable(int pos){
		return table.get(pos);
	}
	public static synchronized void clearTable(){
		table.clear();
	}
	public synchronized static ArrayList<Agent> getTable(){
		return table;
	}
	//for concurrency issues....
	public synchronized static ArrayList<Agent> getCopy(){
		
		ArrayList<Agent> tableCopy = new ArrayList<>();
		for(Agent a : table){
			tableCopy.add(a);
		}
		return tableCopy;
	}

	//Constructor used for testing
	public GameServer(String ip, int port, String name, String gIP, int gP){
		
		nameServer = ip;
		namePort = port;
		hallName = name;
		gameIP = gIP;
		gamePort = gP;
		
	}
	
	public static void main(String[] args) throws IOException {
			
		//if arguments provided use them otherwise ask for info
		if(args.length == 5){
		nameServer = args[0];
		namePort = Integer.parseInt(args[1]);
		hallName = args[2];
		gameIP = args[3];
		gamePort = Integer.parseInt(args[4]);
		}
		
		BufferedReader conIn = new BufferedReader(new InputStreamReader(System.in));

	    //ask for servername host and IP
		//if no args ask for details I know its alot but we have lots of threads..
		if(nameServer == null){
		System.out.println("Enter NameServer IP/Domain if used");
		nameServer = conIn.readLine();
		System.out.println("Please enter Port: ");
		namePort = Integer.parseInt(conIn.readLine());
		System.out.println("Please enter IP for GameServer: ");
		gameIP = conIn.readLine();
		System.out.println("Please enter name for GameServer");
		hallName = conIn.readLine();
		System.out.println("Enter Port for GameHall");
		gamePort = Integer.parseInt(conIn.readLine());
		}
		
		Socket nameServerSocket;
		XStream xstream = new XStream();
		
		//register with Register Server
		ServerDetail me = new ServerDetail(hallName,gameIP,gamePort,4);
	
		
		try {
			//create alias for correct unmarshalling at the other end
			xstream.alias("serverDetail", ServerDetail.class);
			String request = xstream.toXML(me);
			//create socket to server
	    	nameServerSocket = new Socket(nameServer,namePort);

			BufferedReader fromServer = new BufferedReader(new InputStreamReader(nameServerSocket.getInputStream()));
			PrintStream toServer = new PrintStream(nameServerSocket.getOutputStream());
			//send serverDetail and end with end_of_xml design decision to avoid misc network
			//miscommunication
			toServer.println(request);
			toServer.println("end_of_xml");			
			System.out.println(fromServer.readLine());
			
		} catch (IOException ns) {
			System.out.println("Server refused connection or is not online!");
		}

		// start of ConnectionListener for requests.
		try {
			ConnectionListener t = new ConnectionListener(gamePort,hallName);
			t.start();

			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));

			System.out.println("Sebastian Zaba's Gamer Server");
			
			while(! in.readLine().contains("quit")){
			System.out.println("Send Agents to vote for this Server Name first:");
			String name = in.readLine();
			
			Agent vote = new Agent(name, hallName, gamePort, 200, 2, 1, hallName);
						
			GameHall.shipAgent(vote,nameServer,namePort);
			}
					
			
		//	if (in.readLine().contains("quit"));
			controlSwitch = false;

		} catch (IOException e) {
			System.out.println("ups in main");
		}
	}

}
//for incoming Agents start GameHall
//should implement queue to wait for a set amount of agents and then start the Gamehall
class ConnectionListener extends Thread {

	boolean controlSwitch = true;
	int port;
	String hallName;
	public ConnectionListener(int port, String hName){
		this.port = port;
		this.hallName = hName;
	}
	
	public void run() {
		int q_len = 6;
		Socket sock;

		try {
			ServerSocket servsock = new ServerSocket(port, q_len);
			while (controlSwitch) {
				sock = servsock.accept(); // servsock stays open waiting for
											// incoming request
				GameHall g = new GameHall(sock, hallName);
				
				g.start();
				g.join(); //wait for thread to die
				
			}
			servsock.close();
		} catch (IOException | InterruptedException ioe) {
			System.out.println(ioe);
		}
	}

}
//Gamehall simulates games and updates agents favorite server and chipstack
//shipping them back after set amount of games have been played.
class GameHall extends Thread {

	Socket hosock;
	static int tSize = 2; //table size
	private String hallName;
	private Agent player = new Agent();
	
	GameHall(Socket hs, String hName) {
		this.hosock = hs;
		setHallName(hName);
	}
	//synchronized used to avoid concurrency issues due to global varaibles
	public synchronized void setHallName(String hall){
		hallName = hall;
	}

	public synchronized String getHallName(){
		return hallName;
	}
	//let us know somebody is here
	public void run() {
		System.out.println("Player Joined!");

		//receive XML and turn it into Agent
		try {
			
			PrintStream out = new PrintStream(hosock.getOutputStream()); // print																		// stream																		// browser
			BufferedReader in = new BufferedReader(new InputStreamReader(
					hosock.getInputStream())); // get info

			String temp;
			String xml = "";
			final String newLine = System.getProperty("line.separator");

			while (true) {
				temp = in.readLine();
				if (temp.indexOf("end_of_xml") > -1)
					break;
				else
					xml = xml + temp + newLine; // Should use StringBuilder in
												// 1.5
			}

			XStream xstream = new XStream(); // XStream used for converting
												// object to XML and vice versa

			player = new Agent();
			xstream.alias("agentDetail", Agent.class);
			player = (Agent) xstream.fromXML(xml);
		//	System.out.println(player.getGameHistory());			
			player.addGameHistory(getHallName());
				
			out.println("Registered: " + player.getAgentName());
			System.out.println(player.getAgentName() + " just Registered with: " + player.getChipStack());
			
			//add player to gameList

			GameServer.gameList.add(player);
		//	}
			//after player is registered add name to game history
			//player.addGameHistory("UeberCool");
			System.out.println("Hall has: " + GameServer.gameList.size()+ " players!");
			
			
			//when enough players have arrived sit them on table and remove from Que
			if(GameServer.gameList.size()>= tSize){
				int i;
				for(i =1; i <= tSize; i++)
				GameServer.addTable(GameServer.gameList.poll());			
			}
			
			//when enough players have arrived start game			
			if (GameServer.getTable().size() == 2) {
				
			
				ArrayList<Integer> initial = new ArrayList<>();
				
				//get chipstack before game begins to determine if they won or lost
				for( Agent p : GameServer.getTable()){				
					 initial.add(p.getChipStack());					
				}
							
				// play games while the agents game counter is not 0
				int i = 0;				
				//might have to expand if tSize is increased
				while (GameServer.getTable(0).getBetSize() != i || 
						GameServer.getTable(1).getBetSize() != i) {
					
					//no skill winners are random
					Random generator = new Random();
					//generate a random win amount
					int win = generator.nextInt(20);
					//get random winner
					int winner = generator.nextInt(tSize);
					
					//average bid from each player
					int bid = win / GameServer.getTable().size();
					
					for(Agent bids : GameServer.getCopy()){					
						bids.setChipStack((bids.getChipStack()-bid));	//check if this is working				
					}
					//get current total
					int getInitial = GameServer.getTable(winner).getChipStack();
					//increment stack by winnings
					int total = getInitial + win;
					System.out.println(winner + " " + total +"winnings:" + win);
					GameServer.getTable(winner).setChipStack(total);
					i++;
				}
				//after game is over check for winners and set status to 0 for bad and 1 for winners
				int numPlayer = 0;
				for(Agent players : GameServer.getCopy()){					
					if( players.getChipStack()> initial.get(numPlayer)){		
							players.setStatus(1);
							players.setFav(hallName);
							
					}
					else{
						players.setStatus(0);
						
					}
					i++;
				}
				//send agents back to Hive			
				for(Agent bye : GameServer.getCopy()){
					
					shipAgent(bye, GameServer.nameServer, GameServer.namePort);
				}
				
				System.out.println("Shipped!!!");
				GameServer.clearTable();
				initial.clear();
			}

		} catch (IOException e) {
		
			e.printStackTrace();
		}

	}
	//not used for later expansion
	public Agent getAgent(Agent searched) {

		if (GameServer.getTable().contains(searched)) {
			int re = GameServer.getTable().indexOf(searched);
			return GameServer.getTable(re);
		}
		return null;
	}

	// ship agent back to Register
	public static void shipAgent(Agent getmeoutofhere, String serverName, int port) {

		XStream xstream = new XStream();
		xstream.alias("agentDetail", Agent.class);
		String XMLAgent = xstream.toXML(getmeoutofhere);

		Socket sock;
		try {
			sock = new Socket(serverName, port);

			BufferedReader fromServer = new BufferedReader(
					new InputStreamReader(sock.getInputStream()));
			PrintStream toServer = new PrintStream(sock.getOutputStream());

			toServer.println(XMLAgent);
			toServer.println("end_of_xml");
			fromServer.readLine();

		} catch (IOException e) {
			
			e.printStackTrace();
		}

	}
}



