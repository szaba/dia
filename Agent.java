
import java.util.ArrayList;

//Agent class used to be shipped around and keep track of its travel history
//The game history is a permanent mark until agent dies
//Index 0 in GameHistory is for the foodsource(ABC algorithm)
public class Agent {

	public Agent(String aN, String i, int p, int cS,int nG, int s, String v){
		setAgentName(aN);
		setIP(i);
		setPort(p);
		setChipStack(cS);
		setBetSize(nG);
		setStatus (s);	
		addGameHistory(v);
	}

	public Agent(){}
		
	private String agentName;
	private String IP;
	private int port;
	private int chipStack;
	private int betSize;
	private int status;
	private ArrayList<String> gameHistory = new ArrayList<String>();
	//private String foodSource; //only shared with friendly agents
	
	public synchronized String getAgentName() {
		return agentName;
	}

	public synchronized void setAgentName(String aName) {
		agentName = aName;
	}
    public synchronized void setIP(String ip){
    	IP = ip;
    }
    public synchronized String getIP(){
    	return IP;
    }
    public synchronized void setPort(int p){
    	port = p;
    }
    public synchronized int getPort(){
    	return port;
    }
	public synchronized int getChipStack() {
		return chipStack;
	}
	
	public synchronized void setChipStack(int cStack) {
		chipStack = cStack;
	}

	public synchronized int getBetSize() {
		return betSize;
	}

	public synchronized void setBetSize(int bSize) {
		betSize = bSize;
	}

	public synchronized int getStatus() {
		return status;
	}

	public synchronized void setStatus(int s) {
		status = s;
	}

	public synchronized ArrayList<String> getGameHistory() {
		return gameHistory;
	}

	public synchronized void addGameHistory(String serverID){
		gameHistory.add(serverID);
	}
	public synchronized void setFav(String server){
		gameHistory.set(0, server);
	}
public String toString(){
	
	String sb = "";
	for(String gH: gameHistory){
		sb.concat(gH);
	}
	
	return sb;
	
}
	}

