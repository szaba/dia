


public class ServerDetail{
	
private String serverName;
private String serverIP;
private int serverPort;
private int maxAgents;
public ServerDetail(){}; //for xStream library to create object from xml

public ServerDetail(String id, String add, int hole, int mA){
	
	setName(id);
	setIP(add);
	setPort(hole);
	setMaxAgents(mA);
	
}
	
public void setName(String n){this.serverName = n;}

public String getName(){return serverName;}

public void setIP(String i){this.serverIP = i;}

public String getIP(){return serverIP;}

public void setPort(int p){this.serverPort = p;}

public int getPort(){return serverPort;}

public int getMaxAgents() {
	return maxAgents;
}

public void setMaxAgents(int maxAgents) {
	this.maxAgents = maxAgents;
}
	
}