/*Demo Driver for DIA
 * by Sebastian Zaba
 * This will start the Register then the two GameServers which will register with the Register.
 * Then the DemoDriver will create 4 agents and send them to Register to get the game started.
 * 
*/

import java.io.*;
import java.net.*;

import com.thoughtworks.xstream.XStream;

public class DemoDriver {

	
	public static void main(String[] args) {
			
		String server;
		
			if(args.length > 0){
			server = args[0];					
			}
			else server = "localhost";
							
			sentString("SecretAgent",server);				
															
	}	
	
	//method to send command to server at port 80 takes argument string server and string message
	static void sentString(String name, String serverName){
			
		XStream xStream = new XStream();

		for(int i= 0; i<10; i++){
		try {
			Socket sock = new Socket(serverName, 2555);

	//		BufferedReader fromServer = new BufferedReader(new InputStreamReader(sock.getInputStream()));	
			PrintStream toServer = new PrintStream(sock.getOutputStream());
					
			xStream.alias("agentDetail", Agent.class);
			
		
			Agent serv = new Agent(name+i,"", 0000, 100, 2, 0, "started in DemoDriver");
			String send = xStream.toXML(serv);
			System.out.println(send);
			toServer.println(send);
			toServer.println("end_of_xml");
			toServer.flush();
			sock.close();
			
			
		}catch(IOException t){System.out.println("Timeout");}	
		}
	}
}

